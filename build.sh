#!/bin/bash

source ./.vbles

docker build --build-arg DISTRO=$DISTRIBUTION --build-arg RELEASE=$RELEASE -t $DISTRIBUTION/$RELEASE/docker:$version .
