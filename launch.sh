#!/bin/bash

source ./.vbles

xhost +local:

NAME=${CONTAINERNAME#* }

if [[ -z "$NAME" ]]; then
	echo -e "\n\t--name argument is mandatory to locate the docker directory\n"
	exit -1
fi

docker container run -it --privileged=True $CONTAINERNAME --rm \
    --cpus="1" --memory="1g" \
    -e DISPLAY=$DISPLAY -e QT_X11_NO_MITSHM=1 \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $HOME/var/lib/docker.$NAME:/var/lib/docker \
    -v $HOME/src:/src \
    $DISTRIBUTION/$RELEASE/docker:$version

